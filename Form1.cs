﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalPortfolio
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double[] price = new double[100];
        string[] inst = new string[100];
        string[] insttype = new string[100];
        string[] dire = new string[100];
        double[] quantity = new double[100];
        double[] tp = new double[100];
        double[] mp = new double[100];
        double[]  delta = new double[100];
        double[] gamma = new double[100];
        double[] vega = new double[100];
        double[] theta = new double[100];
        double[] rho = new double[100];



        private void btnInstAdd_Click(object sender, EventArgs e)
        {
            Properties.FinalEntityContainer pleaseWork = new Properties.FinalEntityContainer();
            pleaseWork.Instruments.Add(new Properties.Instrument
            {
                CompanyName = InstCN.Text,
                Ticker = InstTick.Text
            });
            pleaseWork.SaveChanges();
        }

        private void btnStockAdd_Click(object sender, EventArgs e)
        {
            Properties.FinalEntityContainer pleaseWork = new Properties.FinalEntityContainer();
            pleaseWork.Stocks.Add(new Properties.Stock
            {
                CompanyName = tbStockCn.Text,
                UnderlyingPrice = Convert.ToDouble(tbStockUP.Text),
                Date = textBox1.Text
            });
            pleaseWork.SaveChanges();
        }
        int i = -1;
        private void btnOptAdd_Click(object sender, EventArgs e)
        {
            string Type, bartype, cp;
            bartype = "N/A";
            double rebate, bar;
            bar = 0;
            rebate = 0;
            if (rbOptCall.Checked == true)
            {
                cp = "Call";
            }
            else
            {
                cp = "Put";
            }
            if (rbOptEuro.Checked == true)
            {
                Type = "European";
            }
            else if (rbOptLook.Checked == true)
            {
                Type = "Lookback";
            }
            else if (rbOptAsian.Checked == true)
            {
                Type = "Asian";
            }
            else if (rbOptBar.Checked == true)
            {
                Type = "Barrier";
                bar = Convert.ToDouble(tbOptBar.Text);
                if (rbOptUandO.Checked == true)
                {
                    bartype = "Up and Out";
                }
                else if (rbOptUandI.Checked == true)
                {
                    bartype = "Up and In";
                }
                else if (rbOptDandI.Checked == true)
                {
                    bartype = "Down and In";
                }
                else if (rbOptDandO.Checked == true)
                {
                    bartype = "Down and Out";
                }
            }
            else if (rbOptRange.Checked == true)
            {
                Type = "Range";

            }
            else
            {
                Type = "Digital";
                rebate = Convert.ToDouble(tbOptRebate.Text);

            }
            Properties.FinalEntityContainer pleaseWork = new Properties.FinalEntityContainer();
            pleaseWork.Options.Add(new Properties.Option
            {
                CompanyName = tbOptCN.Text,
                UnderlyingPrice = Convert.ToDouble(tbOptUP.Text),
                Strike = Convert.ToDouble(tbOptS.Text),
                Tenor = Convert.ToDouble(tbOptTen.Text),
                OptionType = Type,
                Barrier = bar,
                Rebate = rebate,
                BarType = bartype,
                CallPut = cp
            });
            pleaseWork.SaveChanges();
        }

        private void btnHistAdd_Click(object sender, EventArgs e)
        {
            Properties.FinalEntityContainer pleaseWork = new Properties.FinalEntityContainer();
            pleaseWork.HistPrices.Add(new Properties.HistPrice
            {
                StockId = Convert.ToInt32(tbHistCN.Text),
                Price = Convert.ToDouble(tbHistUnd.Text),
                Date = tbHistDat.Text
            });
            pleaseWork.SaveChanges();
        }

        private void btnTradeAdd_Click(object sender, EventArgs e)
        {
            string Type, bartype, cp, direction;
            bartype = "N/A";
            cp = "N/A";

            double rebate, bar, strike, tenor;
            int type;
            bar = 0;
            rebate = 0;
            i++;
            inst[i] = tbTrCN.Text;
            if (rbBuy.Checked == true)
            {
                dire[i] = "Buy";
                direction = "Buy";
            }
            else
            {
                dire[i] = "Sell";
                direction = "Sell";
            }
            
            quantity[i] = Convert.ToDouble(tbTradeQuan.Text);
            if (rbTRSt.Checked == true)
            {
                Type = "Stock";
                insttype[i] = "Stock";
                strike = 0;
                tenor = 0;
                delta[i] = 1;
                gamma[i] = 0;
                vega[i] = 0;
                theta[i] = 0;
                rho[i] = 0;
                if (direction == "Sell" )
                {
                    price[i] = (Convert.ToDouble(tbTradePrice.Text) - Convert.ToDouble(tbTrUP.Text)) * Convert.ToDouble(tbTradeQuan.Text);
                }
                else
                {
                    price[i] = (Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                }

                tp[i] = Convert.ToDouble(tbTradePrice.Text);
                mp[i] = Convert.ToDouble(tbTrUP.Text);
            }
            else
            {
                strike = Convert.ToDouble(tbTrStrike.Text);
                tenor = Convert.ToDouble(tbTrTenor.Text);

                if (rbtrCall.Checked == true)
                {
                    cp = "Call";
                    type = 0;
                }
                else
                {
                    cp = "Put";
                    type = 1;
                }
                if (rbTrEuro.Checked == true)
                {
                    Type = "European Option";
                    if (direction == "Buy")
                    {
                        price[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - EuroPricing(Convert.ToDouble(tbTrUP), strike, Convert.ToDouble(tbvol), Convert.ToDouble(tbIntRate), tenor, 100, 10000, type)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "European Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type);
                    delta[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text)*0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - EuroPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) + EuroPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - 2*mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 *  Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001* Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor*0.001, 100, 10000, type) - mp[i]) * -1 / (0.001 * tenor) ;
                    rho[i] = (EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001* Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - EuroPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text)*0.001, tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
                else if (rbTrLook.Checked == true)
                {
                    Type = "Lookback Option";
                    if (direction == "Buy")
                    {
                        price[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "Lookback Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type);
                    delta[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - LookPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) + LookPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - 2 * mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor * 0.001, 100, 10000, type) - mp[i]) * -1 / (0.001 * tenor);
                    rho[i] = (LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001 * Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - LookPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text) * 0.001, tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
                else if (rbTrAsi.Checked == true)
                {
                    Type = "Asian Option";
                    if (direction == "Buy")
                    {
                        price[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "Asian Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type);
                    delta[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - AsianPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) + AsianPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - 2 * mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor * 0.001, 100, 10000, type) - mp[i]) * -1 / (0.001 * tenor);
                    rho[i] = (AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001 * Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - AsianPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text) * 0.001, tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
                else if (rbTrBar.Checked == true)
                {
                    Type = "Barrier Option";
                    bar = Convert.ToDouble(tbOptBar.Text);
                    int bt = 0;
                    if (rbTrUanO.Checked == true)
                    {
                        bartype = "Up and Out";
                        bt = 1;
                    }
                    else if (rbTrUandI.Checked == true)
                    {
                        bartype = "Up and In";
                        bt = 0;
                    }
                    else if (rbTrDandI.Checked == true)
                    {
                        bartype = "Down and In";
                        bt = 2;
                    }
                    else if (rbTrDandO.Checked == true)
                    {
                        bartype = "Down and Out";
                        bt = 3;
                    }
                    if (direction == "Buy")
                    {
                        price[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,bt,bar) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,bt,bar)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "Barrier Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,bt,bar);
                    delta[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar) - BarPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar) + BarPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar) - 2 * mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar) - BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor * 0.001, 100, 10000, type, bt,bar) - mp[i]) * -1 / (0.001 * tenor);
                    rho[i] = (BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001 * Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, bt,bar) - BarPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text) * 0.001, tenor, 100, 10000, type, bt,bar)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
                else if (rBTrRan.Checked == true)
                {
                    Type = "Range Option";
                    if (direction == "Buy")
                    {
                        price[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "Range Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type);
                    delta[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - RangePricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) + RangePricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - 2 * mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor * 0.001, 100, 10000, type) - mp[i]) * -1 / (0.001 * tenor);
                    rho[i] = (RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001 * Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type) - RangePricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text) * 0.001, tenor, 100, 10000, type)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
                else
                {
                    Type = "Digital Option";
                    rebate = Convert.ToDouble(tbOptRebate.Text);
                    if (direction == "Buy")
                    {
                        price[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,rebate) - Convert.ToDouble(tbTradePrice.Text)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    else
                    {
                        price[i] = (Convert.ToDouble(tbTradePrice.Text) - DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,rebate)) * Convert.ToDouble(tbTradeQuan.Text);

                    }
                    insttype[i] = "Digital Option";
                    tp[i] = Convert.ToDouble(tbTradePrice.Text);
                    mp[i] = DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type,rebate);
                    delta[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate) - DigitalPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate)) / (2 * 0.001 * Convert.ToDouble(tbTrUP.Text));
                    gamma[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text) + Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate) + DigitalPricing(Convert.ToDouble(tbTrUP.Text) - Convert.ToDouble(tbTrUP.Text) * 0.001, strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate) - 2 * mp[i]) / (Math.Pow(Convert.ToDouble(tbTrUP.Text) * 0.001, 2));
                    vega[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) + 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate) - DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text) - 0.001 * Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate)) / (2 * 0.001 * Convert.ToDouble(tbvol.Text));
                    theta[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text), tenor + tenor * 0.001, 100, 10000, type, rebate) - mp[i]) * -1 / (0.001 * tenor);
                    rho[i] = (DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) + 0.001 * Convert.ToDouble(tbIntRate.Text), tenor, 100, 10000, type, rebate) - DigitalPricing(Convert.ToDouble(tbTrUP.Text), strike, Convert.ToDouble(tbvol.Text), Convert.ToDouble(tbIntRate.Text) - Convert.ToDouble(tbIntRate.Text) * 0.001, tenor, 100, 10000, type, rebate)) / (2 * 0.001 * Convert.ToDouble(tbIntRate.Text));

                }
            }
            Properties.FinalEntityContainer pleaseWork = new Properties.FinalEntityContainer();
            pleaseWork.Trades.Add(new Properties.Trade
            {
                CompanyName = tbTrCN.Text,
                UnderlyingPrice = Convert.ToDouble(tbTrUP.Text),
                Direction = direction,
                Quantity = Convert.ToDouble(tbTradeQuan.Text),
                TradePrice = Convert.ToDouble(tbTradePrice.Text),
                InstType = Type,
                Strike = strike,
                Tenor = tenor,
                Barrier = bar,
                Rebate = rebate,
                BarrierType = bartype,
                CallPut = cp
            });
            pleaseWork.SaveChanges();
        }
        public void add()
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            listView1.Items.Clear();

            for (int j = 0; j < i+1; j++)
            {
                string[] row = new string[12];
                row[0] = inst[j];
                row[1] = insttype[j];
                row[2] = dire[j];
                row[3] = Convert.ToString(quantity[j]);
                row[4] = Convert.ToString(tp[j]);
                row[5] = Convert.ToString(mp[j]);
                row[6] = Convert.ToString(price[j]);
                row[7] = Convert.ToString(delta[j]);
                row[8] = Convert.ToString(gamma[j]);
                row[9] = Convert.ToString(vega[j]);
                row[10] = Convert.ToString(theta[j]);
                row[11] = Convert.ToString(rho[j]);

                ListViewItem item = new ListViewItem(row);
                listView1.Items.Add(item);
            }

            
            
        }


        public double[,] PolarReject()
        {
            //Declare Variables

            double[,] prand = new double[10002, 100];
            double rand;
            double rand2;
            double w;
            double c;
            Random rnd = new Random();
            // Create a for loop that creates a matrix of random numbers
            for (int a = 0; a < 100; a++)
            {
                for (int b = 0; b <= 10001 - 1; b += 2)
                {

                    // Create a do while function that will refind rand and rand2 if w>1
                    do
                    {
                        rand = 2 * rnd.NextDouble() - 1;
                        rand2 = 2 * rnd.NextDouble() - 1;
                        w = rand * rand + rand2 * rand2;
                    }
                    while (w > 1);
                    //Run the arithmatic to get normally distributed RV's
                    c = Math.Sqrt(-2 * Math.Log(w) / w);
                    prand[b, a] = c * rand;
                    prand[b + 1, a] = c * rand2;

                }
            }
            //Return the matrix of doubles 
            return prand;
        }
        public double[,] PathSim(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths)
        {
            double[,] payoff = new double[paths + 1, steps];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            for (int i = 0; i < paths + 1; i++)
            {
                payoff[i, 0] = s;
            }

            for (int j = 0; j < paths + 1; j++)
            {
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                }
            }
            return payoff;
        }
        public double[] BarSim(double[,] rand, double s, double k, double v, double r, double T, int steps, int paths, int bartype, double bar, int type)
        {
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps1 = Convert.ToDouble(steps);
            double t = T / steps1;
            for (int i = 0; i < paths + 1; i++)
            {
                payoff[i, 0] = s;
            }

            for (int j = 0; j < paths; j++)
            {
                double max = s;
                double min = s;
                for (int i = 0; i < steps - 1; i++)
                {
                    payoff[j, i + 1] = payoff[j, i] * Math.Pow(Math.E, ((r - ((v * v) / 2)) * t) + ((v * Math.Sqrt(t)) * rand[j, i]));
                    if (max < payoff[j, i + 1])
                    {
                        max = payoff[j, i + 1];
                    }
                    if (min > payoff[j, i + 1])
                    {
                        min = payoff[j, i + 1];
                    }
                }
                if (bartype == 0 && max >= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else if (type == 1)
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);

                    }

                }
                else if (bartype == 0)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1 && max >= bar)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 1)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2 && min <= bar)
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
                else if (bartype == 2)
                {
                    payoff2[j] = 0;
                }
                else if (bartype == 3 && min <= bar)
                {
                    payoff2[j] = 0;
                }
                else
                {
                    if (type == 0)
                    {
                        payoff2[j] = Math.Max(payoff[j, steps - 1] - k, 0);
                    }
                    else
                    {
                        payoff2[j] = Math.Max(k - payoff[j, steps - 1], 0);
                    }
                }
            }
            return payoff2;
        }
        public double EuroPricing(double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double vj = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[,] rand = new double[10003, 100];
            rand = PolarReject();
            payoff = PathSim(rand, s, k, v, r, T, steps, paths);

            if (type == 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff[i, steps - 1] - k > 0)
                    {
                        vj += payoff[i, steps - 1] - k;

                    }
                    else
                    {
                        vj += 0;
                        payoff[i, steps - 1] = 0;
                    }
                }
            }
            else if (type == 1)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff[i, steps - 1] > 0)
                    {
                        vj += k - payoff[i, steps - 1];

                    }
                    else
                    {
                        vj += 0;

                    }
                }
            }
            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }
        public double LookPricing(double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double vj = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            double[,] rand = PolarReject();

            payoff = PathSim(rand, s, k, v, r, T, steps, paths);


            if (type == 0)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    payoff2[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] > payoff2[i])
                        {
                            payoff2[i] = payoff[i, j];
                        }

                    }

                }
            }
            else if (type == 1)
            {
                for (int i = 0; i < paths + 1; i++)
                {
                    payoff2[i] = s;
                    for (int j = 1; j < steps; j++)
                    {
                        if (payoff[i, j] < payoff2[i])
                        {
                            payoff2[i] = payoff[i, j];
                        }

                    }

                }
            }


            if (type == 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff2[i] - k > 0)
                    {
                        vj += payoff2[i] - k;
                        payoff2[i] = payoff2[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff2[i] > 0)
                    {
                        vj += k - payoff2[i];
                        payoff2[i] = k - payoff2[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }

            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }
        public double AsianPricing(double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double vj = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            double[,] rand = PolarReject();

            payoff = PathSim(rand, s, k, v, r, T, steps, paths);


            for (int i = 0; i < paths + 1; i++)
            {
                for (int j = 0; j < steps; j++)
                {
                    payoff2[i] += payoff[i, j];
                }
                payoff2[i] = payoff2[i] / steps2;
            }



            if (type == 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff2[i] - k > 0)
                    {
                        vj += payoff2[i] - k;
                        payoff2[i] = payoff2[i] - k;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (k - payoff2[i] > 0)
                    {
                        vj += k - payoff2[i];
                        payoff2[i] = k - payoff2[i];
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }

            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }
        public double DigitalPricing(double s, double k, double v, double r, double T, int steps, int paths, int type, double rebate)
        {
            double vj = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths];
            double steps2 = Convert.ToDouble(steps);

            double[,] rand = PolarReject();

            payoff = PathSim(rand, s, k, v, r, T, steps, paths);



            if (type == 0)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff[i, steps - 1] >= k)
                    {
                        vj += rebate;
                        payoff2[i] = rebate;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }
            else if (type == 1)
            {
                for (int i = 0; i < paths; i++)
                {
                    if (payoff[i, steps - 1] <= k)
                    {
                        vj += rebate;
                        payoff2[i] = rebate;
                    }
                    else
                    {
                        vj += 0;
                        payoff2[i] = 0;
                    }
                }
            }


            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }
        public double RangePricing(double s, double k, double v, double r, double T, int steps, int paths, int type)
        {
            double vj = 0;
            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double[] payoff3 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            double[,] rand = PolarReject();

            payoff = PathSim(rand, s, k, v, r, T, steps, paths);


            for (int i = 0; i < paths + 1; i++)
            {
                payoff2[i] = s;
                for (int j = 1; j < steps; j++)
                {
                    if (payoff[i, j] > payoff2[i])
                    {
                        payoff2[i] = payoff[i, j];
                    }

                }

            }

            for (int i = 0; i < paths + 1; i++)
            {
                payoff3[i] = s;
                for (int j = 1; j < steps; j++)
                {
                    if (payoff[i, j] < payoff3[i])
                    {
                        payoff3[i] = payoff[i, j];
                    }

                }

            }
            for (int i = 0; i < paths; i++)
            {

                vj += payoff2[i] - payoff3[i];
                payoff2[i] = payoff2[i] - payoff3[i];

            }


            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }
        public double BarPricing(double s, double k, double v, double r, double T, int steps, int paths, int type, int bartype, double bar)
        {
            double vj = 0;

            double[] err = new double[(paths + 1) / 2];
            double[] answer = new double[2];
            double[,] payoff = new double[paths + 1, steps];
            double[] payoff2 = new double[paths + 1];
            double[] payoff3 = new double[paths + 1];
            double steps2 = Convert.ToDouble(steps);
            double[,] rand = PolarReject();

            payoff2 = BarSim(rand, s, k, v, r, T, steps, paths, bartype, bar, type);
            

            for (int i = 0; i < payoff2.Length; i++)
            {
                vj += payoff2[i];
            }

            answer[0] = (vj / paths) * Math.Pow(Math.E, -r * T);
            return answer[0];
        }

        private void btnInstDel_Click(object sender, EventArgs e)
        {


        }

        private void btnStockdel_Click(object sender, EventArgs e)
        {

        }

        private void btnHistChange_Click(object sender, EventArgs e)
        {

        }

        private void btnOptDel_Click(object sender, EventArgs e)
        {

        }

        private void btnTradeDel_Click(object sender, EventArgs e)
        {

        }
    }
}