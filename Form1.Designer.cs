﻿namespace FinalPortfolio
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRefresh = new System.Windows.Forms.Button();
            this.gbTrade = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tbTrRebate = new System.Windows.Forms.TextBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rbTrDandO = new System.Windows.Forms.RadioButton();
            this.rbTrDandI = new System.Windows.Forms.RadioButton();
            this.rbTrUandI = new System.Windows.Forms.RadioButton();
            this.rbTrUanO = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.tbTrbar = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tbTrStrike = new System.Windows.Forms.TextBox();
            this.tbTrTenor = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.rbTrPut = new System.Windows.Forms.RadioButton();
            this.rbtrCall = new System.Windows.Forms.RadioButton();
            this.rbTrBar = new System.Windows.Forms.RadioButton();
            this.rbTrDig = new System.Windows.Forms.RadioButton();
            this.rbTrAsi = new System.Windows.Forms.RadioButton();
            this.rBTrRan = new System.Windows.Forms.RadioButton();
            this.rbTrLook = new System.Windows.Forms.RadioButton();
            this.rbTrEuro = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rbTrOpt = new System.Windows.Forms.RadioButton();
            this.rbTRSt = new System.Windows.Forms.RadioButton();
            this.label14 = new System.Windows.Forms.Label();
            this.tbTrCN = new System.Windows.Forms.TextBox();
            this.tbTrUP = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btnTradeDel = new System.Windows.Forms.Button();
            this.btnTradeAdd = new System.Windows.Forms.Button();
            this.tbTradeQuan = new System.Windows.Forms.TextBox();
            this.tbTradePrice = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.tbvol = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbIntRate = new System.Windows.Forms.TextBox();
            this.gbOpt = new System.Windows.Forms.GroupBox();
            this.rbOptPut = new System.Windows.Forms.RadioButton();
            this.rbOptCall = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rbOptBar = new System.Windows.Forms.RadioButton();
            this.rbOptDig = new System.Windows.Forms.RadioButton();
            this.rbOptAsian = new System.Windows.Forms.RadioButton();
            this.rbOptRange = new System.Windows.Forms.RadioButton();
            this.rbOptLook = new System.Windows.Forms.RadioButton();
            this.rbOptEuro = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rbOptDandO = new System.Windows.Forms.RadioButton();
            this.rbOptDandI = new System.Windows.Forms.RadioButton();
            this.rbOptUandI = new System.Windows.Forms.RadioButton();
            this.rbOptUandO = new System.Windows.Forms.RadioButton();
            this.label29 = new System.Windows.Forms.Label();
            this.tbOptBar = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.tbOptRebate = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tbOptCN = new System.Windows.Forms.TextBox();
            this.tbOptUP = new System.Windows.Forms.TextBox();
            this.btnOptDel = new System.Windows.Forms.Button();
            this.btnOptAdd = new System.Windows.Forms.Button();
            this.tbOptS = new System.Windows.Forms.TextBox();
            this.tbOptTen = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.gbStock = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbStockCn = new System.Windows.Forms.TextBox();
            this.tbStockUP = new System.Windows.Forms.TextBox();
            this.btnStockdel = new System.Windows.Forms.Button();
            this.btnStockAdd = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.gbHist = new System.Windows.Forms.GroupBox();
            this.tbHistCN = new System.Windows.Forms.TextBox();
            this.tbHistUnd = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnHistChange = new System.Windows.Forms.Button();
            this.btnHistAdd = new System.Windows.Forms.Button();
            this.tbHistDat = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.gbINst = new System.Windows.Forms.GroupBox();
            this.btnInstDel = new System.Windows.Forms.Button();
            this.btnInstAdd = new System.Windows.Forms.Button();
            this.InstTick = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.InstCN = new System.Windows.Forms.TextBox();
            this.listView1 = new System.Windows.Forms.ListView();
            this.Instrument = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.InstrumentType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Direction = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Quantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TradePrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.MarkPrice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PL = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Delta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gamma = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Vega = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Theta = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Rho = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.rbBuy = new System.Windows.Forms.RadioButton();
            this.rbSell = new System.Windows.Forms.RadioButton();
            this.gbTrade.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.gbOpt.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.gbStock.SuspendLayout();
            this.gbHist.SuspendLayout();
            this.gbINst.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(399, 987);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(278, 49);
            this.btnRefresh.TabIndex = 36;
            this.btnRefresh.Text = "Refresh Trades";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gbTrade
            // 
            this.gbTrade.Controls.Add(this.rbSell);
            this.gbTrade.Controls.Add(this.rbBuy);
            this.gbTrade.Controls.Add(this.groupBox13);
            this.gbTrade.Controls.Add(this.groupBox12);
            this.gbTrade.Controls.Add(this.groupBox10);
            this.gbTrade.Controls.Add(this.groupBox9);
            this.gbTrade.Controls.Add(this.label14);
            this.gbTrade.Controls.Add(this.tbTrCN);
            this.gbTrade.Controls.Add(this.tbTrUP);
            this.gbTrade.Controls.Add(this.label15);
            this.gbTrade.Controls.Add(this.btnTradeDel);
            this.gbTrade.Controls.Add(this.btnTradeAdd);
            this.gbTrade.Controls.Add(this.tbTradeQuan);
            this.gbTrade.Controls.Add(this.tbTradePrice);
            this.gbTrade.Controls.Add(this.label5);
            this.gbTrade.Controls.Add(this.label8);
            this.gbTrade.Location = new System.Drawing.Point(1117, 39);
            this.gbTrade.Name = "gbTrade";
            this.gbTrade.Size = new System.Drawing.Size(489, 903);
            this.gbTrade.TabIndex = 34;
            this.gbTrade.TabStop = false;
            this.gbTrade.Text = "Trade";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label16);
            this.groupBox13.Controls.Add(this.tbTrRebate);
            this.groupBox13.Location = new System.Drawing.Point(154, 730);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(200, 84);
            this.groupBox13.TabIndex = 36;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Digital Options";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 39);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 20);
            this.label16.TabIndex = 9;
            this.label16.Text = "Rebate:";
            // 
            // tbTrRebate
            // 
            this.tbTrRebate.Location = new System.Drawing.Point(94, 36);
            this.tbTrRebate.Name = "tbTrRebate";
            this.tbTrRebate.Size = new System.Drawing.Size(100, 26);
            this.tbTrRebate.TabIndex = 12;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.rbTrDandO);
            this.groupBox12.Controls.Add(this.rbTrDandI);
            this.groupBox12.Controls.Add(this.rbTrUandI);
            this.groupBox12.Controls.Add(this.rbTrUanO);
            this.groupBox12.Controls.Add(this.label7);
            this.groupBox12.Controls.Add(this.tbTrbar);
            this.groupBox12.Location = new System.Drawing.Point(25, 571);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(435, 153);
            this.groupBox12.TabIndex = 35;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Barrier Options";
            // 
            // rbTrDandO
            // 
            this.rbTrDandO.AutoSize = true;
            this.rbTrDandO.Location = new System.Drawing.Point(244, 114);
            this.rbTrDandO.Name = "rbTrDandO";
            this.rbTrDandO.Size = new System.Drawing.Size(136, 24);
            this.rbTrDandO.TabIndex = 15;
            this.rbTrDandO.TabStop = true;
            this.rbTrDandO.Text = "Down and Out";
            this.rbTrDandO.UseVisualStyleBackColor = true;
            // 
            // rbTrDandI
            // 
            this.rbTrDandI.AutoSize = true;
            this.rbTrDandI.Location = new System.Drawing.Point(244, 84);
            this.rbTrDandI.Name = "rbTrDandI";
            this.rbTrDandI.Size = new System.Drawing.Size(124, 24);
            this.rbTrDandI.TabIndex = 14;
            this.rbTrDandI.TabStop = true;
            this.rbTrDandI.Text = "Down and In";
            this.rbTrDandI.UseVisualStyleBackColor = true;
            // 
            // rbTrUandI
            // 
            this.rbTrUandI.AutoSize = true;
            this.rbTrUandI.Location = new System.Drawing.Point(244, 54);
            this.rbTrUandI.Name = "rbTrUandI";
            this.rbTrUandI.Size = new System.Drawing.Size(104, 24);
            this.rbTrUandI.TabIndex = 13;
            this.rbTrUandI.TabStop = true;
            this.rbTrUandI.Text = "Up and In";
            this.rbTrUandI.UseVisualStyleBackColor = true;
            // 
            // rbTrUanO
            // 
            this.rbTrUanO.AutoSize = true;
            this.rbTrUanO.Location = new System.Drawing.Point(244, 22);
            this.rbTrUanO.Name = "rbTrUanO";
            this.rbTrUanO.Size = new System.Drawing.Size(116, 24);
            this.rbTrUanO.TabIndex = 12;
            this.rbTrUanO.TabStop = true;
            this.rbTrUanO.Text = "Up and Out";
            this.rbTrUanO.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 2;
            this.label7.Text = "Barrier:";
            // 
            // tbTrbar
            // 
            this.tbTrbar.Location = new System.Drawing.Point(95, 43);
            this.tbTrbar.Name = "tbTrbar";
            this.tbTrbar.Size = new System.Drawing.Size(100, 26);
            this.tbTrbar.TabIndex = 11;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tbTrStrike);
            this.groupBox10.Controls.Add(this.tbTrTenor);
            this.groupBox10.Controls.Add(this.label17);
            this.groupBox10.Controls.Add(this.label18);
            this.groupBox10.Controls.Add(this.groupBox11);
            this.groupBox10.Controls.Add(this.rbTrBar);
            this.groupBox10.Controls.Add(this.rbTrDig);
            this.groupBox10.Controls.Add(this.rbTrAsi);
            this.groupBox10.Controls.Add(this.rBTrRan);
            this.groupBox10.Controls.Add(this.rbTrLook);
            this.groupBox10.Controls.Add(this.rbTrEuro);
            this.groupBox10.Location = new System.Drawing.Point(27, 265);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(435, 270);
            this.groupBox10.TabIndex = 34;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Option Type";
            // 
            // tbTrStrike
            // 
            this.tbTrStrike.Location = new System.Drawing.Point(88, 223);
            this.tbTrStrike.Name = "tbTrStrike";
            this.tbTrStrike.Size = new System.Drawing.Size(100, 26);
            this.tbTrStrike.TabIndex = 22;
            // 
            // tbTrTenor
            // 
            this.tbTrTenor.Location = new System.Drawing.Point(295, 223);
            this.tbTrTenor.Name = "tbTrTenor";
            this.tbTrTenor.Size = new System.Drawing.Size(100, 26);
            this.tbTrTenor.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(21, 223);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 20);
            this.label17.TabIndex = 20;
            this.label17.Text = "Strike:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(229, 226);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 20);
            this.label18.TabIndex = 19;
            this.label18.Text = "Tenor:";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.rbTrPut);
            this.groupBox11.Controls.Add(this.rbtrCall);
            this.groupBox11.Location = new System.Drawing.Point(105, 124);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(256, 72);
            this.groupBox11.TabIndex = 18;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Call or Put";
            // 
            // rbTrPut
            // 
            this.rbTrPut.AutoSize = true;
            this.rbTrPut.Location = new System.Drawing.Point(154, 25);
            this.rbTrPut.Name = "rbTrPut";
            this.rbTrPut.Size = new System.Drawing.Size(58, 24);
            this.rbTrPut.TabIndex = 35;
            this.rbTrPut.TabStop = true;
            this.rbTrPut.Text = "Put";
            this.rbTrPut.UseVisualStyleBackColor = true;
            // 
            // rbtrCall
            // 
            this.rbtrCall.AutoSize = true;
            this.rbtrCall.Location = new System.Drawing.Point(48, 25);
            this.rbtrCall.Name = "rbtrCall";
            this.rbtrCall.Size = new System.Drawing.Size(60, 24);
            this.rbtrCall.TabIndex = 34;
            this.rbtrCall.TabStop = true;
            this.rbtrCall.Text = "Call";
            this.rbtrCall.UseVisualStyleBackColor = true;
            // 
            // rbTrBar
            // 
            this.rbTrBar.AutoSize = true;
            this.rbTrBar.Location = new System.Drawing.Point(303, 83);
            this.rbTrBar.Name = "rbTrBar";
            this.rbTrBar.Size = new System.Drawing.Size(81, 24);
            this.rbTrBar.TabIndex = 17;
            this.rbTrBar.TabStop = true;
            this.rbTrBar.Text = "Barrier";
            this.rbTrBar.UseVisualStyleBackColor = true;
            // 
            // rbTrDig
            // 
            this.rbTrDig.AutoSize = true;
            this.rbTrDig.Location = new System.Drawing.Point(304, 38);
            this.rbTrDig.Name = "rbTrDig";
            this.rbTrDig.Size = new System.Drawing.Size(78, 24);
            this.rbTrDig.TabIndex = 16;
            this.rbTrDig.TabStop = true;
            this.rbTrDig.Text = "Digital";
            this.rbTrDig.UseVisualStyleBackColor = true;
            // 
            // rbTrAsi
            // 
            this.rbTrAsi.AutoSize = true;
            this.rbTrAsi.Location = new System.Drawing.Point(165, 86);
            this.rbTrAsi.Name = "rbTrAsi";
            this.rbTrAsi.Size = new System.Drawing.Size(74, 24);
            this.rbTrAsi.TabIndex = 15;
            this.rbTrAsi.TabStop = true;
            this.rbTrAsi.Text = "Asian";
            this.rbTrAsi.UseVisualStyleBackColor = true;
            // 
            // rBTrRan
            // 
            this.rBTrRan.AutoSize = true;
            this.rBTrRan.Location = new System.Drawing.Point(19, 86);
            this.rBTrRan.Name = "rBTrRan";
            this.rBTrRan.Size = new System.Drawing.Size(82, 24);
            this.rBTrRan.TabIndex = 14;
            this.rBTrRan.TabStop = true;
            this.rBTrRan.Text = "Range";
            this.rBTrRan.UseVisualStyleBackColor = true;
            // 
            // rbTrLook
            // 
            this.rbTrLook.AutoSize = true;
            this.rbTrLook.Location = new System.Drawing.Point(166, 41);
            this.rbTrLook.Name = "rbTrLook";
            this.rbTrLook.Size = new System.Drawing.Size(103, 24);
            this.rbTrLook.TabIndex = 13;
            this.rbTrLook.TabStop = true;
            this.rbTrLook.Text = "Lookback";
            this.rbTrLook.UseVisualStyleBackColor = true;
            // 
            // rbTrEuro
            // 
            this.rbTrEuro.AutoSize = true;
            this.rbTrEuro.Location = new System.Drawing.Point(19, 38);
            this.rbTrEuro.Name = "rbTrEuro";
            this.rbTrEuro.Size = new System.Drawing.Size(104, 24);
            this.rbTrEuro.TabIndex = 12;
            this.rbTrEuro.TabStop = true;
            this.rbTrEuro.Text = "European";
            this.rbTrEuro.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rbTrOpt);
            this.groupBox9.Controls.Add(this.rbTRSt);
            this.groupBox9.Location = new System.Drawing.Point(27, 168);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(433, 91);
            this.groupBox9.TabIndex = 33;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Instrument Type";
            // 
            // rbTrOpt
            // 
            this.rbTrOpt.AutoSize = true;
            this.rbTrOpt.Location = new System.Drawing.Point(243, 45);
            this.rbTrOpt.Name = "rbTrOpt";
            this.rbTrOpt.Size = new System.Drawing.Size(81, 24);
            this.rbTrOpt.TabIndex = 19;
            this.rbTrOpt.TabStop = true;
            this.rbTrOpt.Text = "Option";
            this.rbTrOpt.UseVisualStyleBackColor = true;
            // 
            // rbTRSt
            // 
            this.rbTRSt.AutoSize = true;
            this.rbTRSt.Location = new System.Drawing.Point(105, 48);
            this.rbTRSt.Name = "rbTRSt";
            this.rbTRSt.Size = new System.Drawing.Size(75, 24);
            this.rbTRSt.TabIndex = 18;
            this.rbTRSt.TabStop = true;
            this.rbTRSt.Text = "Stock";
            this.rbTRSt.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 20);
            this.label14.TabIndex = 31;
            this.label14.Text = "Company Name:";
            // 
            // tbTrCN
            // 
            this.tbTrCN.Location = new System.Drawing.Point(132, 31);
            this.tbTrCN.Name = "tbTrCN";
            this.tbTrCN.Size = new System.Drawing.Size(100, 26);
            this.tbTrCN.TabIndex = 32;
            // 
            // tbTrUP
            // 
            this.tbTrUP.Location = new System.Drawing.Point(132, 76);
            this.tbTrUP.Name = "tbTrUP";
            this.tbTrUP.Size = new System.Drawing.Size(100, 26);
            this.tbTrUP.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 20);
            this.label15.TabIndex = 29;
            this.label15.Text = "Underlying Price:";
            // 
            // btnTradeDel
            // 
            this.btnTradeDel.Location = new System.Drawing.Point(260, 840);
            this.btnTradeDel.Name = "btnTradeDel";
            this.btnTradeDel.Size = new System.Drawing.Size(171, 32);
            this.btnTradeDel.TabIndex = 26;
            this.btnTradeDel.Text = "Delete Trade";
            this.btnTradeDel.UseVisualStyleBackColor = true;
            this.btnTradeDel.Click += new System.EventHandler(this.btnTradeDel_Click);
            // 
            // btnTradeAdd
            // 
            this.btnTradeAdd.Location = new System.Drawing.Point(49, 840);
            this.btnTradeAdd.Name = "btnTradeAdd";
            this.btnTradeAdd.Size = new System.Drawing.Size(171, 31);
            this.btnTradeAdd.TabIndex = 27;
            this.btnTradeAdd.Text = "Add Trade";
            this.btnTradeAdd.UseVisualStyleBackColor = true;
            this.btnTradeAdd.Click += new System.EventHandler(this.btnTradeAdd_Click);
            // 
            // tbTradeQuan
            // 
            this.tbTradeQuan.Location = new System.Drawing.Point(360, 76);
            this.tbTradeQuan.Name = "tbTradeQuan";
            this.tbTradeQuan.Size = new System.Drawing.Size(100, 26);
            this.tbTradeQuan.TabIndex = 22;
            // 
            // tbTradePrice
            // 
            this.tbTradePrice.Location = new System.Drawing.Point(360, 28);
            this.tbTradePrice.Name = "tbTradePrice";
            this.tbTradePrice.Size = new System.Drawing.Size(100, 26);
            this.tbTradePrice.TabIndex = 21;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(261, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 20);
            this.label5.TabIndex = 20;
            this.label5.Text = "Trade Price:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Quantity:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(330, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 20);
            this.label13.TabIndex = 32;
            this.label13.Text = "Volatility %";
            // 
            // tbvol
            // 
            this.tbvol.Location = new System.Drawing.Point(439, 26);
            this.tbvol.Name = "tbvol";
            this.tbvol.Size = new System.Drawing.Size(100, 26);
            this.tbvol.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 20);
            this.label1.TabIndex = 30;
            this.label1.Text = "Interest Rate";
            // 
            // tbIntRate
            // 
            this.tbIntRate.Location = new System.Drawing.Point(144, 23);
            this.tbIntRate.Name = "tbIntRate";
            this.tbIntRate.Size = new System.Drawing.Size(100, 26);
            this.tbIntRate.TabIndex = 31;
            // 
            // gbOpt
            // 
            this.gbOpt.Controls.Add(this.rbOptPut);
            this.gbOpt.Controls.Add(this.rbOptCall);
            this.gbOpt.Controls.Add(this.groupBox8);
            this.gbOpt.Controls.Add(this.groupBox7);
            this.gbOpt.Controls.Add(this.groupBox6);
            this.gbOpt.Controls.Add(this.label12);
            this.gbOpt.Controls.Add(this.tbOptCN);
            this.gbOpt.Controls.Add(this.tbOptUP);
            this.gbOpt.Controls.Add(this.btnOptDel);
            this.gbOpt.Controls.Add(this.btnOptAdd);
            this.gbOpt.Controls.Add(this.tbOptS);
            this.gbOpt.Controls.Add(this.tbOptTen);
            this.gbOpt.Controls.Add(this.label34);
            this.gbOpt.Controls.Add(this.label33);
            this.gbOpt.Controls.Add(this.label30);
            this.gbOpt.Controls.Add(this.label31);
            this.gbOpt.Location = new System.Drawing.Point(567, 26);
            this.gbOpt.Name = "gbOpt";
            this.gbOpt.Size = new System.Drawing.Size(507, 605);
            this.gbOpt.TabIndex = 27;
            this.gbOpt.TabStop = false;
            this.gbOpt.Text = "Option";
            // 
            // rbOptPut
            // 
            this.rbOptPut.AutoSize = true;
            this.rbOptPut.Location = new System.Drawing.Point(294, 511);
            this.rbOptPut.Name = "rbOptPut";
            this.rbOptPut.Size = new System.Drawing.Size(58, 24);
            this.rbOptPut.TabIndex = 33;
            this.rbOptPut.TabStop = true;
            this.rbOptPut.Text = "Put";
            this.rbOptPut.UseVisualStyleBackColor = true;
            // 
            // rbOptCall
            // 
            this.rbOptCall.AutoSize = true;
            this.rbOptCall.Location = new System.Drawing.Point(148, 511);
            this.rbOptCall.Name = "rbOptCall";
            this.rbOptCall.Size = new System.Drawing.Size(60, 24);
            this.rbOptCall.TabIndex = 32;
            this.rbOptCall.TabStop = true;
            this.rbOptCall.Text = "Call";
            this.rbOptCall.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.rbOptBar);
            this.groupBox8.Controls.Add(this.rbOptDig);
            this.groupBox8.Controls.Add(this.rbOptAsian);
            this.groupBox8.Controls.Add(this.rbOptRange);
            this.groupBox8.Controls.Add(this.rbOptLook);
            this.groupBox8.Controls.Add(this.rbOptEuro);
            this.groupBox8.Location = new System.Drawing.Point(35, 105);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(435, 135);
            this.groupBox8.TabIndex = 31;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Option Type";
            // 
            // rbOptBar
            // 
            this.rbOptBar.AutoSize = true;
            this.rbOptBar.Location = new System.Drawing.Point(303, 83);
            this.rbOptBar.Name = "rbOptBar";
            this.rbOptBar.Size = new System.Drawing.Size(81, 24);
            this.rbOptBar.TabIndex = 17;
            this.rbOptBar.TabStop = true;
            this.rbOptBar.Text = "Barrier";
            this.rbOptBar.UseVisualStyleBackColor = true;
            // 
            // rbOptDig
            // 
            this.rbOptDig.AutoSize = true;
            this.rbOptDig.Location = new System.Drawing.Point(304, 38);
            this.rbOptDig.Name = "rbOptDig";
            this.rbOptDig.Size = new System.Drawing.Size(78, 24);
            this.rbOptDig.TabIndex = 16;
            this.rbOptDig.TabStop = true;
            this.rbOptDig.Text = "Digital";
            this.rbOptDig.UseVisualStyleBackColor = true;
            // 
            // rbOptAsian
            // 
            this.rbOptAsian.AutoSize = true;
            this.rbOptAsian.Location = new System.Drawing.Point(165, 86);
            this.rbOptAsian.Name = "rbOptAsian";
            this.rbOptAsian.Size = new System.Drawing.Size(74, 24);
            this.rbOptAsian.TabIndex = 15;
            this.rbOptAsian.TabStop = true;
            this.rbOptAsian.Text = "Asian";
            this.rbOptAsian.UseVisualStyleBackColor = true;
            // 
            // rbOptRange
            // 
            this.rbOptRange.AutoSize = true;
            this.rbOptRange.Location = new System.Drawing.Point(19, 86);
            this.rbOptRange.Name = "rbOptRange";
            this.rbOptRange.Size = new System.Drawing.Size(82, 24);
            this.rbOptRange.TabIndex = 14;
            this.rbOptRange.TabStop = true;
            this.rbOptRange.Text = "Range";
            this.rbOptRange.UseVisualStyleBackColor = true;
            // 
            // rbOptLook
            // 
            this.rbOptLook.AutoSize = true;
            this.rbOptLook.Location = new System.Drawing.Point(166, 41);
            this.rbOptLook.Name = "rbOptLook";
            this.rbOptLook.Size = new System.Drawing.Size(103, 24);
            this.rbOptLook.TabIndex = 13;
            this.rbOptLook.TabStop = true;
            this.rbOptLook.Text = "Lookback";
            this.rbOptLook.UseVisualStyleBackColor = true;
            // 
            // rbOptEuro
            // 
            this.rbOptEuro.AutoSize = true;
            this.rbOptEuro.Location = new System.Drawing.Point(19, 38);
            this.rbOptEuro.Name = "rbOptEuro";
            this.rbOptEuro.Size = new System.Drawing.Size(104, 24);
            this.rbOptEuro.TabIndex = 12;
            this.rbOptEuro.TabStop = true;
            this.rbOptEuro.Text = "European";
            this.rbOptEuro.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.rbOptDandO);
            this.groupBox7.Controls.Add(this.rbOptDandI);
            this.groupBox7.Controls.Add(this.rbOptUandI);
            this.groupBox7.Controls.Add(this.rbOptUandO);
            this.groupBox7.Controls.Add(this.label29);
            this.groupBox7.Controls.Add(this.tbOptBar);
            this.groupBox7.Location = new System.Drawing.Point(35, 246);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(435, 153);
            this.groupBox7.TabIndex = 30;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Barrier Options";
            // 
            // rbOptDandO
            // 
            this.rbOptDandO.AutoSize = true;
            this.rbOptDandO.Location = new System.Drawing.Point(244, 114);
            this.rbOptDandO.Name = "rbOptDandO";
            this.rbOptDandO.Size = new System.Drawing.Size(136, 24);
            this.rbOptDandO.TabIndex = 15;
            this.rbOptDandO.TabStop = true;
            this.rbOptDandO.Text = "Down and Out";
            this.rbOptDandO.UseVisualStyleBackColor = true;
            // 
            // rbOptDandI
            // 
            this.rbOptDandI.AutoSize = true;
            this.rbOptDandI.Location = new System.Drawing.Point(244, 84);
            this.rbOptDandI.Name = "rbOptDandI";
            this.rbOptDandI.Size = new System.Drawing.Size(124, 24);
            this.rbOptDandI.TabIndex = 14;
            this.rbOptDandI.TabStop = true;
            this.rbOptDandI.Text = "Down and In";
            this.rbOptDandI.UseVisualStyleBackColor = true;
            // 
            // rbOptUandI
            // 
            this.rbOptUandI.AutoSize = true;
            this.rbOptUandI.Location = new System.Drawing.Point(244, 54);
            this.rbOptUandI.Name = "rbOptUandI";
            this.rbOptUandI.Size = new System.Drawing.Size(104, 24);
            this.rbOptUandI.TabIndex = 13;
            this.rbOptUandI.TabStop = true;
            this.rbOptUandI.Text = "Up and In";
            this.rbOptUandI.UseVisualStyleBackColor = true;
            // 
            // rbOptUandO
            // 
            this.rbOptUandO.AutoSize = true;
            this.rbOptUandO.Location = new System.Drawing.Point(244, 22);
            this.rbOptUandO.Name = "rbOptUandO";
            this.rbOptUandO.Size = new System.Drawing.Size(116, 24);
            this.rbOptUandO.TabIndex = 12;
            this.rbOptUandO.TabStop = true;
            this.rbOptUandO.Text = "Up and Out";
            this.rbOptUandO.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 20);
            this.label29.TabIndex = 2;
            this.label29.Text = "Barrier:";
            // 
            // tbOptBar
            // 
            this.tbOptBar.Location = new System.Drawing.Point(95, 43);
            this.tbOptBar.Name = "tbOptBar";
            this.tbOptBar.Size = new System.Drawing.Size(100, 26);
            this.tbOptBar.TabIndex = 11;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label36);
            this.groupBox6.Controls.Add(this.tbOptRebate);
            this.groupBox6.Location = new System.Drawing.Point(155, 405);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 100);
            this.groupBox6.TabIndex = 29;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Digital Options";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(18, 39);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 20);
            this.label36.TabIndex = 9;
            this.label36.Text = "Rebate:";
            // 
            // tbOptRebate
            // 
            this.tbOptRebate.Location = new System.Drawing.Point(94, 36);
            this.tbOptRebate.Name = "tbOptRebate";
            this.tbOptRebate.Size = new System.Drawing.Size(100, 26);
            this.tbOptRebate.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 38);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(126, 20);
            this.label12.TabIndex = 27;
            this.label12.Text = "Company Name:";
            // 
            // tbOptCN
            // 
            this.tbOptCN.Location = new System.Drawing.Point(165, 34);
            this.tbOptCN.Name = "tbOptCN";
            this.tbOptCN.Size = new System.Drawing.Size(100, 26);
            this.tbOptCN.TabIndex = 28;
            // 
            // tbOptUP
            // 
            this.tbOptUP.Location = new System.Drawing.Point(165, 74);
            this.tbOptUP.Name = "tbOptUP";
            this.tbOptUP.Size = new System.Drawing.Size(100, 26);
            this.tbOptUP.TabIndex = 26;
            // 
            // btnOptDel
            // 
            this.btnOptDel.Location = new System.Drawing.Point(279, 550);
            this.btnOptDel.Name = "btnOptDel";
            this.btnOptDel.Size = new System.Drawing.Size(171, 32);
            this.btnOptDel.TabIndex = 24;
            this.btnOptDel.Text = "Delete Option";
            this.btnOptDel.UseVisualStyleBackColor = true;
            this.btnOptDel.Click += new System.EventHandler(this.btnOptDel_Click);
            // 
            // btnOptAdd
            // 
            this.btnOptAdd.Location = new System.Drawing.Point(26, 551);
            this.btnOptAdd.Name = "btnOptAdd";
            this.btnOptAdd.Size = new System.Drawing.Size(171, 31);
            this.btnOptAdd.TabIndex = 25;
            this.btnOptAdd.Text = "Add Option";
            this.btnOptAdd.UseVisualStyleBackColor = true;
            this.btnOptAdd.Click += new System.EventHandler(this.btnOptAdd_Click);
            // 
            // tbOptS
            // 
            this.tbOptS.Location = new System.Drawing.Point(357, 37);
            this.tbOptS.Name = "tbOptS";
            this.tbOptS.Size = new System.Drawing.Size(100, 26);
            this.tbOptS.TabIndex = 14;
            // 
            // tbOptTen
            // 
            this.tbOptTen.Location = new System.Drawing.Point(357, 76);
            this.tbOptTen.Name = "tbOptTen";
            this.tbOptTen.Size = new System.Drawing.Size(100, 26);
            this.tbOptTen.TabIndex = 13;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(22, 77);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(127, 20);
            this.label34.TabIndex = 7;
            this.label34.Text = "Underlying Price:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(286, 37);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 20);
            this.label33.TabIndex = 6;
            this.label33.Text = "Strike:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(10, 281);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(0, 20);
            this.label30.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(286, 76);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(50, 20);
            this.label31.TabIndex = 4;
            this.label31.Text = "Tenor";
            // 
            // gbStock
            // 
            this.gbStock.Controls.Add(this.label2);
            this.gbStock.Controls.Add(this.tbStockCn);
            this.gbStock.Controls.Add(this.tbStockUP);
            this.gbStock.Controls.Add(this.btnStockdel);
            this.gbStock.Controls.Add(this.btnStockAdd);
            this.gbStock.Controls.Add(this.textBox1);
            this.gbStock.Controls.Add(this.label3);
            this.gbStock.Controls.Add(this.label4);
            this.gbStock.Location = new System.Drawing.Point(281, 69);
            this.gbStock.Name = "gbStock";
            this.gbStock.Size = new System.Drawing.Size(255, 313);
            this.gbStock.TabIndex = 28;
            this.gbStock.TabStop = false;
            this.gbStock.Text = "Stock";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "Company Name:";
            // 
            // tbStockCn
            // 
            this.tbStockCn.Location = new System.Drawing.Point(149, 38);
            this.tbStockCn.Name = "tbStockCn";
            this.tbStockCn.Size = new System.Drawing.Size(100, 26);
            this.tbStockCn.TabIndex = 26;
            // 
            // tbStockUP
            // 
            this.tbStockUP.Location = new System.Drawing.Point(149, 83);
            this.tbStockUP.Name = "tbStockUP";
            this.tbStockUP.Size = new System.Drawing.Size(100, 26);
            this.tbStockUP.TabIndex = 24;
            // 
            // btnStockdel
            // 
            this.btnStockdel.Location = new System.Drawing.Point(35, 238);
            this.btnStockdel.Name = "btnStockdel";
            this.btnStockdel.Size = new System.Drawing.Size(171, 32);
            this.btnStockdel.TabIndex = 22;
            this.btnStockdel.Text = "Delete Stock";
            this.btnStockdel.UseVisualStyleBackColor = true;
            this.btnStockdel.Click += new System.EventHandler(this.btnStockdel_Click);
            // 
            // btnStockAdd
            // 
            this.btnStockAdd.Location = new System.Drawing.Point(35, 175);
            this.btnStockAdd.Name = "btnStockAdd";
            this.btnStockAdd.Size = new System.Drawing.Size(171, 31);
            this.btnStockAdd.TabIndex = 23;
            this.btnStockAdd.Text = "Add Stock";
            this.btnStockAdd.UseVisualStyleBackColor = true;
            this.btnStockAdd.Click += new System.EventHandler(this.btnStockAdd_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(149, 125);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 26);
            this.textBox1.TabIndex = 20;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 20);
            this.label3.TabIndex = 18;
            this.label3.Text = "Underlying Price:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(84, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 20);
            this.label4.TabIndex = 17;
            this.label4.Text = "Date:";
            // 
            // gbHist
            // 
            this.gbHist.Controls.Add(this.tbHistCN);
            this.gbHist.Controls.Add(this.tbHistUnd);
            this.gbHist.Controls.Add(this.label11);
            this.gbHist.Controls.Add(this.btnHistChange);
            this.gbHist.Controls.Add(this.btnHistAdd);
            this.gbHist.Controls.Add(this.tbHistDat);
            this.gbHist.Controls.Add(this.label9);
            this.gbHist.Controls.Add(this.label10);
            this.gbHist.Location = new System.Drawing.Point(2, 345);
            this.gbHist.Name = "gbHist";
            this.gbHist.Size = new System.Drawing.Size(263, 272);
            this.gbHist.TabIndex = 29;
            this.gbHist.TabStop = false;
            this.gbHist.Text = "Historical Price";
            // 
            // tbHistCN
            // 
            this.tbHistCN.Location = new System.Drawing.Point(136, 39);
            this.tbHistCN.Name = "tbHistCN";
            this.tbHistCN.Size = new System.Drawing.Size(100, 26);
            this.tbHistCN.TabIndex = 32;
            // 
            // tbHistUnd
            // 
            this.tbHistUnd.Location = new System.Drawing.Point(136, 74);
            this.tbHistUnd.Name = "tbHistUnd";
            this.tbHistUnd.Size = new System.Drawing.Size(100, 26);
            this.tbHistUnd.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(51, 39);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 20);
            this.label11.TabIndex = 30;
            this.label11.Text = "Stock ID";
            // 
            // btnHistChange
            // 
            this.btnHistChange.Location = new System.Drawing.Point(41, 224);
            this.btnHistChange.Name = "btnHistChange";
            this.btnHistChange.Size = new System.Drawing.Size(171, 32);
            this.btnHistChange.TabIndex = 28;
            this.btnHistChange.Text = "Change Price";
            this.btnHistChange.UseVisualStyleBackColor = true;
            this.btnHistChange.Click += new System.EventHandler(this.btnHistChange_Click);
            // 
            // btnHistAdd
            // 
            this.btnHistAdd.Location = new System.Drawing.Point(39, 171);
            this.btnHistAdd.Name = "btnHistAdd";
            this.btnHistAdd.Size = new System.Drawing.Size(171, 31);
            this.btnHistAdd.TabIndex = 29;
            this.btnHistAdd.Text = "Add Price";
            this.btnHistAdd.UseVisualStyleBackColor = true;
            this.btnHistAdd.Click += new System.EventHandler(this.btnHistAdd_Click);
            // 
            // tbHistDat
            // 
            this.tbHistDat.Location = new System.Drawing.Point(136, 109);
            this.tbHistDat.Name = "tbHistDat";
            this.tbHistDat.Size = new System.Drawing.Size(100, 26);
            this.tbHistDat.TabIndex = 26;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(74, 77);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 20);
            this.label9.TabIndex = 25;
            this.label9.Text = "Price:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(74, 115);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 20);
            this.label10.TabIndex = 24;
            this.label10.Text = "Date:";
            // 
            // gbINst
            // 
            this.gbINst.Controls.Add(this.btnInstDel);
            this.gbINst.Controls.Add(this.btnInstAdd);
            this.gbINst.Controls.Add(this.InstTick);
            this.gbINst.Controls.Add(this.label27);
            this.gbINst.Controls.Add(this.label35);
            this.gbINst.Controls.Add(this.InstCN);
            this.gbINst.Location = new System.Drawing.Point(23, 69);
            this.gbINst.Name = "gbINst";
            this.gbINst.Size = new System.Drawing.Size(242, 270);
            this.gbINst.TabIndex = 26;
            this.gbINst.TabStop = false;
            this.gbINst.Text = "Instrument";
            // 
            // btnInstDel
            // 
            this.btnInstDel.Location = new System.Drawing.Point(37, 212);
            this.btnInstDel.Name = "btnInstDel";
            this.btnInstDel.Size = new System.Drawing.Size(171, 32);
            this.btnInstDel.TabIndex = 13;
            this.btnInstDel.Text = "Delete Instrument";
            this.btnInstDel.UseVisualStyleBackColor = true;
            this.btnInstDel.Click += new System.EventHandler(this.btnInstDel_Click);
            // 
            // btnInstAdd
            // 
            this.btnInstAdd.Location = new System.Drawing.Point(37, 143);
            this.btnInstAdd.Name = "btnInstAdd";
            this.btnInstAdd.Size = new System.Drawing.Size(171, 31);
            this.btnInstAdd.TabIndex = 14;
            this.btnInstAdd.Text = "Add Instrument";
            this.btnInstAdd.UseVisualStyleBackColor = true;
            this.btnInstAdd.Click += new System.EventHandler(this.btnInstAdd_Click);
            // 
            // InstTick
            // 
            this.InstTick.Location = new System.Drawing.Point(136, 80);
            this.InstTick.Name = "InstTick";
            this.InstTick.Size = new System.Drawing.Size(100, 26);
            this.InstTick.TabIndex = 15;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(-4, 39);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(126, 20);
            this.label27.TabIndex = 0;
            this.label27.Text = "Company Name:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(67, 80);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(55, 20);
            this.label35.TabIndex = 8;
            this.label35.Text = "Ticker:";
            // 
            // InstCN
            // 
            this.InstCN.Location = new System.Drawing.Point(136, 36);
            this.InstCN.Name = "InstCN";
            this.InstCN.Size = new System.Drawing.Size(100, 26);
            this.InstCN.TabIndex = 10;
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.InstrumentType,
            this.Instrument,
            this.Direction,
            this.Quantity,
            this.TradePrice,
            this.MarkPrice,
            this.PL,
            this.Delta,
            this.Gamma,
            this.Vega,
            this.Theta,
            this.Rho});
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(12, 653);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1075, 303);
            this.listView1.TabIndex = 37;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // Instrument
            // 
            this.Instrument.DisplayIndex = 0;
            this.Instrument.Text = "Instrument";
            this.Instrument.Width = 100;
            // 
            // InstrumentType
            // 
            this.InstrumentType.DisplayIndex = 1;
            this.InstrumentType.Text = "Instrument Type";
            this.InstrumentType.Width = 150;
            // 
            // Direction
            // 
            this.Direction.Text = "Direction";
            this.Direction.Width = 80;
            // 
            // Quantity
            // 
            this.Quantity.Text = "Quantity";
            this.Quantity.Width = 80;
            // 
            // TradePrice
            // 
            this.TradePrice.Text = "Trade Price";
            this.TradePrice.Width = 110;
            // 
            // MarkPrice
            // 
            this.MarkPrice.Text = "Mark Price";
            this.MarkPrice.Width = 100;
            // 
            // PL
            // 
            this.PL.Text = "P&L";
            this.PL.Width = 100;
            // 
            // Delta
            // 
            this.Delta.Text = "Delta";
            this.Delta.Width = 70;
            // 
            // Gamma
            // 
            this.Gamma.Text = "Gamma";
            this.Gamma.Width = 70;
            // 
            // Vega
            // 
            this.Vega.Text = "Vega";
            this.Vega.Width = 70;
            // 
            // Theta
            // 
            this.Theta.Text = "Theta";
            this.Theta.Width = 70;
            // 
            // Rho
            // 
            this.Rho.Text = "Rho";
            this.Rho.Width = 70;
            // 
            // rbBuy
            // 
            this.rbBuy.AutoSize = true;
            this.rbBuy.Location = new System.Drawing.Point(132, 133);
            this.rbBuy.Name = "rbBuy";
            this.rbBuy.Size = new System.Drawing.Size(61, 24);
            this.rbBuy.TabIndex = 37;
            this.rbBuy.TabStop = true;
            this.rbBuy.Text = "Buy";
            this.rbBuy.UseVisualStyleBackColor = true;
            // 
            // rbSell
            // 
            this.rbSell.AutoSize = true;
            this.rbSell.Location = new System.Drawing.Point(259, 133);
            this.rbSell.Name = "rbSell";
            this.rbSell.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rbSell.Size = new System.Drawing.Size(60, 24);
            this.rbSell.TabIndex = 38;
            this.rbSell.TabStop = true;
            this.rbSell.Text = "Sell";
            this.rbSell.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1716, 1156);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.gbTrade);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbvol);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbIntRate);
            this.Controls.Add(this.gbOpt);
            this.Controls.Add(this.gbStock);
            this.Controls.Add(this.gbHist);
            this.Controls.Add(this.gbINst);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gbTrade.ResumeLayout(false);
            this.gbTrade.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.gbOpt.ResumeLayout(false);
            this.gbOpt.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.gbStock.ResumeLayout(false);
            this.gbStock.PerformLayout();
            this.gbHist.ResumeLayout(false);
            this.gbHist.PerformLayout();
            this.gbINst.ResumeLayout(false);
            this.gbINst.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.GroupBox gbTrade;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tbTrRebate;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton rbTrDandO;
        private System.Windows.Forms.RadioButton rbTrDandI;
        private System.Windows.Forms.RadioButton rbTrUandI;
        private System.Windows.Forms.RadioButton rbTrUanO;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbTrbar;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox tbTrStrike;
        private System.Windows.Forms.TextBox tbTrTenor;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton rbTrPut;
        private System.Windows.Forms.RadioButton rbtrCall;
        private System.Windows.Forms.RadioButton rbTrBar;
        private System.Windows.Forms.RadioButton rbTrDig;
        private System.Windows.Forms.RadioButton rbTrAsi;
        private System.Windows.Forms.RadioButton rBTrRan;
        private System.Windows.Forms.RadioButton rbTrLook;
        private System.Windows.Forms.RadioButton rbTrEuro;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton rbTrOpt;
        private System.Windows.Forms.RadioButton rbTRSt;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tbTrCN;
        private System.Windows.Forms.TextBox tbTrUP;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnTradeDel;
        private System.Windows.Forms.Button btnTradeAdd;
        private System.Windows.Forms.TextBox tbTradeQuan;
        private System.Windows.Forms.TextBox tbTradePrice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbvol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbIntRate;
        private System.Windows.Forms.GroupBox gbOpt;
        private System.Windows.Forms.RadioButton rbOptPut;
        private System.Windows.Forms.RadioButton rbOptCall;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton rbOptBar;
        private System.Windows.Forms.RadioButton rbOptDig;
        private System.Windows.Forms.RadioButton rbOptAsian;
        private System.Windows.Forms.RadioButton rbOptRange;
        private System.Windows.Forms.RadioButton rbOptLook;
        private System.Windows.Forms.RadioButton rbOptEuro;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton rbOptDandO;
        private System.Windows.Forms.RadioButton rbOptDandI;
        private System.Windows.Forms.RadioButton rbOptUandI;
        private System.Windows.Forms.RadioButton rbOptUandO;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbOptBar;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbOptRebate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbOptCN;
        private System.Windows.Forms.TextBox tbOptUP;
        private System.Windows.Forms.Button btnOptDel;
        private System.Windows.Forms.Button btnOptAdd;
        private System.Windows.Forms.TextBox tbOptS;
        private System.Windows.Forms.TextBox tbOptTen;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox gbStock;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbStockCn;
        private System.Windows.Forms.TextBox tbStockUP;
        private System.Windows.Forms.Button btnStockdel;
        private System.Windows.Forms.Button btnStockAdd;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox gbHist;
        private System.Windows.Forms.TextBox tbHistCN;
        private System.Windows.Forms.TextBox tbHistUnd;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnHistChange;
        private System.Windows.Forms.Button btnHistAdd;
        private System.Windows.Forms.TextBox tbHistDat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox gbINst;
        private System.Windows.Forms.Button btnInstDel;
        private System.Windows.Forms.Button btnInstAdd;
        private System.Windows.Forms.TextBox InstTick;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox InstCN;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader Instrument;
        private System.Windows.Forms.ColumnHeader InstrumentType;
        private System.Windows.Forms.ColumnHeader Direction;
        private System.Windows.Forms.ColumnHeader Quantity;
        private System.Windows.Forms.ColumnHeader TradePrice;
        private System.Windows.Forms.ColumnHeader MarkPrice;
        private System.Windows.Forms.ColumnHeader PL;
        private System.Windows.Forms.ColumnHeader Delta;
        private System.Windows.Forms.ColumnHeader Gamma;
        private System.Windows.Forms.ColumnHeader Vega;
        private System.Windows.Forms.ColumnHeader Theta;
        private System.Windows.Forms.ColumnHeader Rho;
        private System.Windows.Forms.RadioButton rbSell;
        private System.Windows.Forms.RadioButton rbBuy;
    }
}

