
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/28/2018 15:06:49
-- Generated from EDMX file: c:\users\jacob gotto\source\repos\FinalPortfolio\FinalPortfolio\Properties\FinalEntity.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [FinalPortfolio];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Instruments'
CREATE TABLE [dbo].[Instruments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [Ticker] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Stocks'
CREATE TABLE [dbo].[Stocks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [UnderlyingPrice] float  NOT NULL,
    [Date] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Options'
CREATE TABLE [dbo].[Options] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [CallPut] nvarchar(max)  NOT NULL,
    [OptionType] nvarchar(max)  NOT NULL,
    [BarType] nvarchar(max)  NULL,
    [UnderlyingPrice] float  NOT NULL,
    [Strike] float  NOT NULL,
    [Tenor] float  NOT NULL,
    [Barrier] float  NULL,
    [Rebate] float  NULL
);
GO

-- Creating table 'Trades'
CREATE TABLE [dbo].[Trades] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [CompanyName] nvarchar(max)  NOT NULL,
    [InstType] nvarchar(max)  NOT NULL,
    [Direction] nvarchar(max)  NOT NULL,
    [Quantity] float  NOT NULL,
    [TradePrice] float  NOT NULL,
    [UnderlyingPrice] float  NOT NULL,
    [Strike] float  NULL,
    [Tenor] float  NULL,
    [CallPut] nvarchar(max)  NULL,
    [BarrierType] nvarchar(max)  NULL,
    [Barrier] float  NULL,
    [Rebate] float  NULL
);
GO

-- Creating table 'HistPrices'
CREATE TABLE [dbo].[HistPrices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Price] float  NOT NULL,
    [Date] nvarchar(max)  NOT NULL,
    [StockId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Instruments'
ALTER TABLE [dbo].[Instruments]
ADD CONSTRAINT [PK_Instruments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Stocks'
ALTER TABLE [dbo].[Stocks]
ADD CONSTRAINT [PK_Stocks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Options'
ALTER TABLE [dbo].[Options]
ADD CONSTRAINT [PK_Options]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trades'
ALTER TABLE [dbo].[Trades]
ADD CONSTRAINT [PK_Trades]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HistPrices'
ALTER TABLE [dbo].[HistPrices]
ADD CONSTRAINT [PK_HistPrices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [StockId] in table 'HistPrices'
ALTER TABLE [dbo].[HistPrices]
ADD CONSTRAINT [FK_StockHistPrice]
    FOREIGN KEY ([StockId])
    REFERENCES [dbo].[Stocks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StockHistPrice'
CREATE INDEX [IX_FK_StockHistPrice]
ON [dbo].[HistPrices]
    ([StockId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------